#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetVerticalSync(true);
	
	//bSendSerialMessage = false;
	ofBackground(255);	
	ofSetLogLevel(OF_LOG_VERBOSE);
	
	font.load("DIN.otf", 64);
	
	serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
	
	// this should be set to whatever com port your serial device is connected to.
	// (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
	// arduino users check in arduino app....
	int baud = 115200;
	//serial.setup(0, baud); //open the first device
	//serial.setup("COM4", baud); // windows example
	serial.setup("/dev/cu.usbmodem14201", baud); // mac osx example
	//serial.setup("/dev/ttyUSB0", baud); //linux example
	
	nTimesRead = 0;
	nBytesRead = 0;
	readTime = 0;
	memset(bytesReadString, 0, 4);
    
    /*Initialise message to send to arduino*/
    sendMsg = 1;
    
    /*Audio switch bool*/
    playAudio = false;
    bSendSerialMessage = true;
}

//--------------------------------------------------------------
void ofApp::update(){
    
    if(bSendSerialMessage)
    {
        while(serial.available())
        {
            //serial.writeByte(sendMsg);
            serial.readBytes(receivedData, 10);
           // printf("receivedData is %d \n", receivedData);
        }
    }


	//if (bSendSerialMessage){
		
		// (1) write the letter "a" to serial:
		
		
		// (2) read
		// now we try to read 3 bytes
		// since we might not get them all the time 3 - but sometimes 0, 6, or something else,
		// we will try to read three bytes, as much as we can
		// otherwise, we may have a "lag" if we don't read fast enough
		// or just read three every time. now, we will be sure to 
		// read as much as we can in groups of three...
		
//		nTimesRead = 0;
//		nBytesRead = 0;
//		int nRead  = 0;  // a temp variable to keep count per read
//
//		unsigned char bytesReturned[3];
//
//		memset(bytesReadString, 0, 4);
//		memset(bytesReturned, 0, 3);
//
//		while( (nRead = serial.readBytes( bytesReturned, 3)) > 0){
//			nTimesRead++;
//			nBytesRead = nRead;
//		};
//
//		memcpy(bytesReadString, bytesReturned, 3);
//
//		bSendSerialMessage = false;
//		readTime = ofGetElapsedTimef();
	//}
}

//--------------------------------------------------------------
void ofApp::draw(){
//	if (nBytesRead > 0 && ((ofGetElapsedTimef() - readTime) < 0.5f)){
//		ofSetColor(0);
//	} else {
//		ofSetColor(220);
//	}
//	string msg;
//	//msg += "click to test serial:\n";
//	//msg += "nBytes read " + ofToString(nBytesRead) + "\n";
//	//msg += "nTimes read " + ofToString(nTimesRead) + "\n";
//	msg += "read: " + ofToString(bytesReadString) + "\n";
//	//msg += "(at time " + ofToString(readTime, 3) + ")";
//	font.drawString(msg, 50, 100);
    string direction = ofToString(receivedData);
    ofBackground(255);
    std::cout << direction << '.' << std::endl;
    string msg;
    msg += "Gesture sensor says: ";
    msg += direction;
    ofSetColor(0);
    ofDrawBitmapString(msg, 200, 200);
    
    //string toCheck = ofSplitString(direction, " ");
    /*Check if direction is right or left, and then play the "matching" splash. Checks if the character appears in direction string, if yes the result of it will be greater than 0.*/
    string toR = "r";
    string toL = "l";
    int isRight = ofStringTimesInString(direction, toR);
    int isLeft = ofStringTimesInString(direction, toL);

    if((isRight != 0 || isLeft != 0) && playAudio == false)
    {
        
        
        if(isRight != 0)
        {
            playAudio = true;
            theAudio.load("splash3.wav");
            theAudio.play();
            std::cout << "played right splash" << std::endl;
            bSendSerialMessage = false;
        }
        if(isLeft != 0)
        {
            playAudio = true;
            theAudio.load("splash2.wav");
            theAudio.play();
            std::cout << "played left splash" << std::endl;
            bSendSerialMessage = false;
        }
    }
    else
    {
        playAudio = false;
        bSendSerialMessage = true;
    }
//    if()
//    {
//
//    }
//    else
//    {
//        playAudio = false;
//        bSendSerialMessage = true;
//    }
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){ 
	   // bSendSerialMessage = true;
    
//    if(key == 'r')
//    {
//        rightSplash.play();
//    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){ 
    //bSendSerialMessage = false;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
	
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
//    if(x < ofGetWidth() && x > 0 && y < ofGetHeight() && y > 0)
//    {
//        bSendSerialMessage = true;
//    }

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
	
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
	
}

