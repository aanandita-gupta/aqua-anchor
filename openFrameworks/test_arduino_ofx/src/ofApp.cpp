#include "ofApp.h"
#include <iostream>

//--------------------------------------------------------------
void ofApp::setup(){
    serialMessage = false;
    
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    
    // this should be set to whatever com port your serial device is connected to.
    // (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
    // arduino users check in arduino app....
    int baud = 9600;
    //serial.setup(0, baud); //open the first device
    //serial.setup("COM10", baud); // windows example
    serial.setup("/dev/cu.usbmodem143301", baud); // mac osx example
    //serial.setup("/dev/ttyUSB0", baud); //linux example
}

//--------------------------------------------------------------
void ofApp::update(){
    if (serialMessage) {
        serialMessage = false;
        serial.writeByte(sendData); // sending the data to arduino
        /*How to read data from this example https://openframeworks.cc/documentation/communication/ofSerial/#show_readBytes */
        int bytesArrayOffset = bytesRequired - bytesRemaining;
        serial.readBytes(receivedData, 10); // Getting the data from Arduino
        printf("receivedData is", receivedData);    // Printing in ASCII format
        //%s \n
        
       
    }

}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(0);    // Black background
    ofSetColor(255);    // Text color is white

    string msg;
    msg += "Click to turn LED \n";
    msg += receivedData;
    ofDrawBitmapString(msg, 200, 200); // Write the data on the output window
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    serialMessage = true;
}
