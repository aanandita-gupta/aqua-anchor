#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetVerticalSync(true);
    
    /*Background colour depthtest etc*/
    ofEnableDepthTest();
ofDisableAlphaBlending();
    ofBackground(0,0,0);
    /*Lights*/
    ofSetSmoothLighting(true);
     //Set overall brightness of the "world"
     ofSetGlobalAmbientColor(ofColor(230));
    /*camera position*/
    cam.setPosition(0,0,100);
    
    //bSendSerialMessage = false;
    //ofBackground(255);
    ofSetLogLevel(OF_LOG_VERBOSE);
    
    
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
    
    // this should be set to whatever com port your serial device is connected to.
    // (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
    // arduino users check in arduino app....
    int baud = 9600;
    //serial.setup(0, baud); //open the first device
    //serial.setup("COM4", baud); // windows example
    serial.setup("/dev/cu.usbmodem14201", baud); // mac osx example
    //serial.setup("/dev/ttyUSB0", baud); //linux example
    
    nTimesRead = 0;
    nBytesRead = 0;
    readTime = 0;
    memset(bytesReadString, 0, 4);
    
    /*Initialise message to send to arduino*/
    sendMsg = 1;
    
    /*Switch for gesutre sensor to be checking*/
    registerGestures = false;
    
    /*Audio switch bool and audio initialisation*/
    //downSplash.load("splash4.wav");
    rightSplash.load("splash3.wav");
    leftSplash.load("splash2.wav");
    upDownSplash.load("splash1.wav");
    theStream.load("thestream.wav");
    theStream.setVolume(0.2);
    rightSplash.setVolume(1.0);
    leftSplash.setVolume(1.0);
    upDownSplash.setVolume(1.0);
    /*Loop the stream audio*/
    theStream.setLoop(true);
    theStream.play();
    playAudio = false;
    playSound = ' ';
    
    
    /*SETUP FOR ARTWORK*/
    
    /*Booleans to manipulate artwork initalisation*/
    upMove = false;
    downMove = false;
    rightMove = false;
    leftMove = false;
    
    /*size for mesh*/
    campos = 1;
    
    int meshX = 50;
    int meshY = 50;
    int spacing = 1;

    //for loop to add vertices to the mesh -- how big the size of one square should be
    for(int i = -meshX/2; i < meshX/2; i++)
    {
        for(int j = -meshY/2; j <meshY/2; j++)
        {
            mesh.addVertex(ofPoint(i*spacing, j*spacing, 0));
            
        }
    }
    
    // Set up triangles' indices
    // only loop to -1 so they don't connect back to the beginning of the row
    for(int x = 0; x < meshX - 1; x++) {
        for(int y = 0; y < meshY - 1; y++) {
            int topLeft     = x   + meshX * y;
            int bottomLeft  = x+1 + meshX * y;
            int topRight    = x   + meshX * (y+1);
            int bottomRight = x+1 + meshX * (y+1);

            mesh.addTriangle( topLeft, bottomLeft, topRight );
            mesh.addTriangle( bottomLeft, topRight, bottomRight );

        }
    }
    
    /*Sphere setup*/
    aSphere.setRadius(50);
    aSphere.setPosition(0, 0, 0);
    horRot = 0;
    verRot = 0;
    
    /*Image by <a href="https://pixabay.com/users/alkemade-804941/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">alkemade</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">Pixabay</a>*/
    ofDisableArbTex();
    //ofLoadImage(tex,"sea.jpeg");
    img.load("sea.jpg");
    tex = img.getTexture();
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    aSphere.rotateDeg(verRot,0,1,0);
    aSphere.rotateDeg(horRot, 1,0,0);

    while(serial.available())
    {

        serial.readBytes(receivedData, 10);

    }
    
    /*Raise volume of stream when user is close*/
    if(registerGestures == true)
    {
        theStream.setVolume(0.4);
    }
    
    std::cout << registerGestures << std::endl;
    
    /*Update movement of artwork*/
   /* for(int i = 0;i < mesh.getVertices().size(); i++)
    {
        float x = mesh.getVertex(i).x;
        float y = mesh.getVertex(i).y;
        mesh.setVertex(i, ofVec3f(x,y,getZNoiseValue(x, y)));
    }*/

}

//--------------------------------------------------------------
void ofApp::draw(){

    string direction = ofToString(receivedData);
    //ofBackground(255);
    std::cout << direction << '.' << std::endl;
    string msg;
    msg += "Gesture sensor says: ";
    msg += direction;
    ofSetColor(255);
    ofDrawBitmapString(msg, 200, 200);
    
    
    //string toCheck = ofSplitString(direction, " ");
    /*Check if direction is right or left or up or down, and then play the "matching" splash. Checks if the character appears in direction string, if yes the result of it will be greater than 0.*/
    string toR = "r";
    string toL = "l";
    int isRight = ofStringTimesInString(direction, toR);
    int isLeft = ofStringTimesInString(direction, toL);
    std::cout << "Right: " << isRight << std::endl;
    std::cout << "Left: " << isLeft << std::endl;
    unsigned char stopAudio[1] = {0};
    
    /*Check value being sent from arduino, and then set playSOund to match it. Those values (single letters) will control what state the audio will be in*/
    char *dirForComp = &direction[0];
    playSound = *dirForComp;
    
    /*"Switch on" the gesture checking when the user is close enough to the distance sensor*/
    if(playSound == 'g')
    {
        registerGestures = true;
    }
    
    /*Artwork*/
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    cam.begin();
  //  ofEnableDepthTest();
    tex.bind();
    //img.getTextureReference().bind();
        //mesh.draw();
    aSphere.draw();
    //img.getTextureReference().unbind();
    tex.unbind();
    cam.end();
    
    //std::cout << "PlaySound: " << playSound << std::endl;

    
    std::cout <<"PlaySound: " << *dirForComp << std::endl;
    
    /*If the registerGestures boolean has been switched to true, that means the user is near and it's time to play sounds should a gesture be registered*/
    if(registerGestures == true)
    {
        switch(playSound) {
            case 'r' :
                    //rightSplash.play();
                    std::cout << "played right splash" << std::endl;
                    serial.flush(true);
                    //rightMove = true;
                    /*Make artwork spin anti-clockwise*/
                    verRot = 1;
                    //playSound = ' ';
                   // direction = " ";
                break;
            case 'd' :
                    //upDownSplash.play();
                    std::cout << "played down splash" << std::endl;
                    serial.flush(true);
                    //downMove = true;
                    /*Make artwork spin forwards*/
                    horRot = 1;
                    //playSound = ' ';
                    //direction = " ";
                break;
            case 'l' :
                    //leftSplash.play();
                    std::cout << "played left splash" << std::endl;
                    serial.flush(true);
                    //leftMove = true;
                    /*Make artwork spin clockwise*/
                    verRot = -1;
                    //playSound = ' ';
                    //direction = " ";
                break;
            case 'u' :
                    //upDownSplash.play();
                    std::cout << "played up splash" << std::endl;
                    serial.flush(true);
                    //upMove = true;
                    /*Make artwork spin backwards*/
                    horRot = -1;
                    //playSound = ' ';
                    //direction = " ";
                break;
            /*case ' ' :
                    //serial.flush(false);
                    std::cout << "no sound" << std::endl;
                    verRot = 0;
                    horRot = 0;
                break;*/
              
        }
    }

}

//--------------------------------------------------------------
//adding a noise function to return a float value and make a more flowy mesh
float ofApp::getZNoiseValue(float x, float y) {
  
    if(upMove == true)
    {
        y-=1;
    }
    if(downMove == true)
    {
        y+=1;
    }
    if(rightMove == true)
    {
        x+=1;
    }
    if(leftMove == true)
    {
        x-=1;
    }
    float time = ofGetFrameNum() * 0.005;
    float n1 = ofNoise(x * 0.06, y * 0.005, time) * 5;
    float n2 = ofNoise(x * 0.005, y * 0.008, time) * 7;
    return n1 + n2;
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){
    
   /* if(OF_KEY_UP)
    {
        horRot = -5;
    }
    if(OF_KEY_DOWN)
    {
        horRot = 5;
    }
    if(OF_KEY_LEFT)
    {
        verRot = -5;
    }
    if(OF_KEY_RIGHT)
    {
        verRot = 5;
    }*/

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    //bSendSerialMessage = false;
    //verRot = 0;
 /*   if(OF_KEY_UP)
    {
        horRot = 0;
    }
    if(OF_KEY_DOWN)
    {
        horRot = 0;
    }
    if(OF_KEY_LEFT)
    {
        verRot = 0;
    }
    if(OF_KEY_RIGHT)
    {
        verRot = 0;
    }*/
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){


}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}


