#pragma once

#include "ofMain.h"
#include <iostream>

class ofApp : public ofBaseApp{
    
    public:
        void setup();
        void update();
        void draw();

        void keyPressed  (int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
    
        /*Get z noise value for animation of artwork*/
        float getZNoiseValue(float x, float y);
        
        ofTrueTypeFont        font;

        bool        bSendSerialMessage;            // a flag for sending serial
        char        bytesRead[3];                // data from serial, we will be trying to read 3
        char        bytesReadString[4];            // a string needs a null terminator, so we need 3 + 1 bytes
        int            nBytesRead;                    // how much did we read?
        int            nTimesRead;                    // how many times did we read?
        float        readTime;                    // when did we last read?
        
        ofSerial    serial;
    
//    // we want to read 8 bytes
//    int bytesRequired = 8;
//    unsigned char bytes[bytesRequired];
//    int bytesRemaining = bytesRequired;
    
    char receivedData[10];
    /*Message to send to arduino*/
    unsigned char sendMsg;
    
    /*Switching on splashes, as at first only the distance sensor is active*/
    bool registerGestures;
    
    /*AUDIO*/
    char playSound;
    ofSoundPlayer theStream;
    ofSoundPlayer upDownSplash;
    ofSoundPlayer rightSplash;
    ofSoundPlayer leftSplash;
    bool playAudio;
    
    /*ARTWORK*/
    ofCamera cam;
    ofMesh mesh;
    ofTexture tex;
    ofImage img;
    float campos;
    
    /*trying something new*/
    ofSpherePrimitive aSphere;
    /*Rotation of sphere, to be manipulated by isuer when movement is registered*/
    int horRot;
    int verRot;
    /*Bools for manipulation of artwork*/
    bool rightMove;
    bool leftMove;
    bool downMove;
    bool upMove;
    
};

