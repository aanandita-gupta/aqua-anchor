#include "ofApp.h"

/*Started from the serial example that comes with oF*/
//--------------------------------------------------------------
void ofApp::setup(){
	ofSetVerticalSync(true);
	
	//bSendSerialMessage = false;
	ofBackground(255);	
	ofSetLogLevel(OF_LOG_VERBOSE);
	
	font.load("DIN.otf", 64);
	
	serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
	
	// this should be set to whatever com port your serial device is connected to.
	// (ie, COM4 on a pc, /dev/tty.... on linux, /dev/tty... on a mac)
	// arduino users check in arduino app....
	int baud = 9600;
	//serial.setup(0, baud); //open the first device
	//serial.setup("COM4", baud); // windows example
	serial.setup("/dev/cu.usbmodem14201", baud); // mac osx example
	//serial.setup("/dev/ttyUSB0", baud); //linux example
	
	nTimesRead = 0;
	nBytesRead = 0;
	readTime = 0;
	memset(bytesReadString, 0, 4);
    
    /*Initialise message to send to arduino*/
    sendMsg = 1;
    
    /*Load audio files and set their volume*/
    rightSplash.load("splash3.wav");
    leftSplash.load("splash2.wav");
    theStream.load("thestream.wav");
    theStream.setVolume(0.5);
    rightSplash.setVolume(1.0);
    leftSplash.setVolume(1.0);
    /*Loop the stream audio*/
    theStream.setLoop(true);
    theStream.play();
    /*Initialise char that will hold value to determine what aduio file should be played*/
    playSound = ' ';
}

//--------------------------------------------------------------
void ofApp::update(){
    
    /*Read data from Arduino while serial communication is available*/
    while(serial.available())
    {
        serial.readBytes(receivedData, 10);

    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofBackground(255);
    /*Store the data sent by the Arduino as a string, this string will indicate whether a gesture was made to the left,right,up or down or no gesture was made.*/
    string direction = ofToString(receivedData);
    /*Just showing the direction on the screen, as it is not yet connected to teh led lights so need some indication as well as the sounds.*/
    std::cout << direction << '.' << std::endl;
    string msg;
    msg += "Gesture sensor says: ";
    msg += direction;
    ofSetColor(0);
    ofDrawBitmapString(msg, 200, 200);
    

    /*Check if direction is right or left, and then play the "matching" splash. Checks if the character appears in direction string, if yes the result of it will be greater than 0.*/
    string toR = "r";
    string toL = "l";
    /*Counts how many times the letters 'r' or 'l' come up in the message sent from the arduino, if more than 0 it should trigger a splash sound - This method has been discontinued.*/
    int isRight = ofStringTimesInString(direction, toR);
    int isLeft = ofStringTimesInString(direction, toL);
    std::cout << "Right: " << isRight << std::endl;
    std::cout << "Left: " << isLeft << std::endl;
    unsigned char stopAudio[1] = {0};
    
//    if(isRight > 0 || isLeft > 0)
//    {
//        if(isRight > 0)
//        {
//            playSound = 'R';
//        }
//        if(isLeft > 0)
//        {
//            playSound = 'L';
//        }
//    }
//    else
//    {
//        playSound = ' ';
//    }
    /*This converts the string direction into a char that can then be assigned to the unsigned char (playSound) that we are using for the switch statement.*/
    char *dirForComp = &direction[0];
    playSound = *dirForComp;
    
    std::cout <<"PlaySound: " << playSound << std::endl;
    /*Make sure to only play every other time to give the audio time to to play and not get that buzzing noise.*/
    int amountOfMatchforR = 0;
    int amountOfMatchforL = 0;
    
    /*Switch statement that determines whether a sound should be played or not*/
    switch(playSound) {
        case 'r' :
            amountOfMatchforR++;
//            if(ofGetFrameNum() % 30 == 0)
//            {
                rightSplash.play();
                std::cout << "played right splash" << std::endl;
                serial.flush(true);
                //playSound = ' ';
           // }
            break;
        case 'd' :
                serial.flush(false);
                std::cout << "no sound" << std::endl;
            break;
        case 'l' :
            amountOfMatchforL++;
//            if(ofGetFrameNum() % 30 == 0)
//            {
                leftSplash.play();
                std::cout << "played left splash" << std::endl;
                serial.flush(true);
                //playSound = ' ';
//            }
            break;
        case 'u' :
            serial.flush(false);
            std::cout << "no sound" << std::endl;
            break;
        case ' ' :
            serial.flush(false);
            std::cout << "no sound" << std::endl;
            break;
    }

//    if(isRight != 0 || isLeft != 0)
//    {
//        playAudio = true;
//        if(isRight > 0 /*&& pla*/)
//        {
//            //playAudio = true;
//            rightSplash.play();
//            std::cout << "played right splash" << std::endl;
//            serial.writeBytes(&stopAudio[0],1);
//            //
//            //isRight = 0;
//            //bSendSerialMessage = false;
//        }
//        if(isLeft > 0)
//        {
//            leftSplash.play();
//            std::cout << "played left splash" << std::endl;
//            serial.writeBytes(&stopAudio[0],1);
//            //serial.flush(true);
//        }
//
//    }
//    else
//    {
//        playAudio = false;
//        //bSendSerialMessage = true;
//    }
       
//    if(isLeft != 0 /*&& playAudio == false*/)
//    {
//        playAudio = true;
//        leftSplash.play();
//        std::cout << "played left splash" << std::endl;
//        //isLeft = 0;
//       // bSendSerialMessage = false;
//    }
//    else
//    {
//        playAudio = false;
//        //bSendSerialMessage = true;
//    }
//    if()
//    {
//
//    }
//    else
//    {
//        playAudio = false;
//        bSendSerialMessage = true;
//    }
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){ 
	   // bSendSerialMessage = true;
    
//    if(key == 'r')
//    {
//        rightSplash.play();
//    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){ 
    //bSendSerialMessage = false;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){
	
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
//    if(x < ofGetWidth() && x > 0 && y < ofGetHeight() && y > 0)
//    {
//        bSendSerialMessage = true;
//    }

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
	
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
	
}

