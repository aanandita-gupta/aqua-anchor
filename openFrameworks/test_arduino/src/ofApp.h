#pragma once

#include "ofMain.h"
#include <iostream>

class ofApp : public ofBaseApp{
    
    public:
        void setup();
        void update();
        void draw();
        
        void setupArduino(const int & version);
        void keyPressed(int key);
    
        void mousePressed(int x, int y, int button);
//    void mouseReleased(int x, int y, int button);

        bool serialMessage;            // a flag for sending serial
        char receivedData[10];        // for storing incoming data
        char sendData = 0;    // for sending data
        
//        ofSerial serial;
        ofArduino arduino;
};
