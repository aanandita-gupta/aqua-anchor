#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    /*Load sound files*/
    stream.load("stream.wav");
    splash1.load("splash.wav");
    splash2.load("splash2.wav");
    splash3.load("splash3.wav");

}

//--------------------------------------------------------------
void ofApp::update(){
    
    if(dist == true)
    {
        stream.setLoop(true);
        stream.play();
    }
    if(left == true)
    {
        splash1.play();
    }
    if(right == true)
    {
        splash2.play();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if(key == 'd')
    {
        dist = true;
    }
    if(key == '1')
    {
        left = true;
    }
    if(key == '2')
    {
        right = true;
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
    if(key == 'd')
    {
        dist = false;
    }
    if(key == '1')
    {
        left = false;
    }
    if(key == '2')
    {
        right = false;
    }

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
