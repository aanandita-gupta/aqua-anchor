#pragma once

#include "ofMain.h"
#include <iostream>

class ofApp : public ofBaseApp{
    
    public:
        void setup();
        void update();
        void draw();

        void keyPressed  (int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
        

        /*Based on example code that comes with oF, called serialExample.*/
        ofSerial    serial;

    /*To store message receieved from Arduino*/
    char receivedData[10];
    
    /*Switching on splashes, as at first only the distance sensor is active*/
    bool registerGestures;
    
    /*AUDIO*/
    char playSound;
    ofSoundPlayer theStream;
    ofSoundPlayer upDownSplash;
    ofSoundPlayer rightSplash;
    ofSoundPlayer leftSplash;
    bool playAudio;
    
    /*ARTWORK*/
    ofCamera cam;
    ofTexture tex;
    ofImage img;
    ofSpherePrimitive aSphere;
    /*Rotation of sphere, to be manipulated by isuer when movement is registered*/
    int horRot;
    int verRot;
    
};

