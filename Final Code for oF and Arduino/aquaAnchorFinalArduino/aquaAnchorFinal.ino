
#include <NewPing.h>
#include "Adafruit_APDS9960.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

/*
 * Posted on https://randomnerdtutorials.com
 * created by http://playground.arduino.cc/Code/NewPing
*/
/*Distance sensor code based on tutorial linked above, using NewPing library.*/
#define TRIGGER_PIN 9
#define ECHO_PIN 10
#define MAX_DISTANCE 200

/*NewPing setup of pins and maximum distance*/
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 

/*Boolean, will be false initially and this will mean the distance is checked,
 * when the user is within range it will switch to true 
 and the gesture sensor will start to have an effect.*/
bool inRange = false;

/*Gesture sensor*/
Adafruit_APDS9960 apds;



/*LED strip - Uses Adafruit_Neopixel library*/
int ledPin = 5; /*DI of strip connected on pin 5*/

Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, ledPin, NEO_GRB + NEO_KHZ800);


//Animations, reactions to splashes
//Should switch to true when gesture sensor captures certain movements
bool rightSplash = false;
bool leftSplash = false;
bool upOrDownSplash = false;





void setup() {
    // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  //gestSen.begin(115200);

  /*Check start of gestuer sensor*/
  if(!apds.begin())
  {
    Serial.println("failed to initialize device! Please check your wiring.");
  }
  else
  {
    Serial.println("Device initialized!");
  }

  /*Start strip*/
   strip.begin();
   
  //gesture mode will be entered once proximity mode senses something close,
  //running this in setup rather than when inRange is true in loop to limit strain
  apds.enableProximity(true);
  apds.enableGesture(true);

}

void loop() {
  

  /*Start water stream*/
  waterStream(100);

  if(inRange == false)
  {
    checkDistance();
  }
  else
  {
    //gestSen.listen();

    recGestureSendData();
  }

  Serial.println(" ");



}

void checkDistance()
{
   delay(30);
   unsigned int distance = sonar.ping_cm();

   if(distance < 100)
   {
      Serial.println("g");
      Serial.println(" ");
      delay(100);
      inRange = true;
   }
}

/*This function checks what gesture the user has made, and this will in turn trigger a splash sound (from OF)
 * and a different animation for the led lights.
 */
void recGestureSendData() {



   //read a gesture from the device
   uint8_t gesture = apds.readGesture();
     if(gesture == APDS9960_DOWN)
     {
      upOrDownSplash = true;
      Serial.println("d");
      Serial.println(" ");
      //char myStr[2] = "d";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_UP)
     {
      upOrDownSplash = true;
      Serial.println("u");
      Serial.println(" ");
      //char myStr[2] = "u";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_LEFT)
     {
      leftSplash = true;
      Serial.println("l");
      Serial.println(" ");
      //char myStr[2] = "l";
      //Serial.write(myStr, 1);
     }
     if(gesture == APDS9960_RIGHT)
     {
      rightSplash = true;
      Serial.println("r");
      Serial.println(" ");
      //char myStr[2] = "r";
     // Serial.write(myStr, 1);
     }
      Serial.println(" "); 

}

void waterStream(uint8_t wait) {

  strip.setBrightness(30);
  
/*Splitting strip into sections, 
 * the two side sections can continue the same way throughtout but 
 * the middle one should be manipulated when a user moves their hand over the gesture sensor.
  */
  int leftSection = 19;
  int middleSection = 39;
  int rightSection = 59;
  
  /*Stream loops - based on example, candyChase*/
  for (int j=0; j<20; j++) {  
    //q is lower no than amount of colour varitations to give a more random/natural appearacnce*/
    for (int q=0; q < 3; q++) {
      
      /*Left section*/
      for (uint16_t i=0; i < strip.numPixels()/3; i++) {
        strip.setPixelColor(i, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=2) {
        strip.setPixelColor(j+q , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=3) {
        strip.setPixelColor(i+q, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=5) {
        strip.setPixelColor(i+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

    /*right section*/
      for (uint16_t k=39; k < strip.numPixels(); k++) {
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=2) {
        strip.setPixelColor(40 + j+q , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=3) {
        strip.setPixelColor(k+q, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=5) {
        strip.setPixelColor(k+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }


  /*middle section*/
      if(leftSplash == false && rightSplash == false && upOrDownSplash == false)
      {
        for (uint16_t k=19; k < 39; k++) {
          strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
        }
        for (uint16_t k=19; k < 39; k+=2) {
          strip.setPixelColor(19 + j+q , 0,0,205);    //turn every third pixel medium blue
        }
        for (uint16_t k=19; k < 39; k+=3) {
          strip.setPixelColor(k+q, 0,0,205 );    //turn every fifth pixel medium blue
        }
        for (uint16_t k=19; k < 39; k+=5) {
          strip.setPixelColor(k+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
        }
      }   /*If user has moved hand to left, do leftSplash animation*/
      else if(leftSplash == true)
      {
        splashLeft(j, q);
      } /*If user has moved hand to right, do rightSplash animation*/
          else if(rightSplash == true)
      {
        splashRight(j, q);
      } /*If user has moved hand up or down, do upOrDownSplash animation*/
      else if(upOrDownSplash == true)
      {
        splashUpDown(j, q);
      }
  
         strip.show();


      delay(wait);
     }
  }
}


/*Splash animation functions*/
/*left splash*/
void splashLeft(int a, int b)
{
  /*Where the splash should start*/
  int midStrip = 29;

  strip.setBrightness(30);

  /*Background colour on left side of middle, underneath splash*/
  for(int i = 0; i < 10;i++)
  {
    strip.setPixelColor(midStrip - i, 30,144,255); 
  }
      /*The right side, no splash*/
      for (uint16_t k=midStrip; k < 39; k++) {
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=midStrip; k < 39; k+=2) {
        strip.setPixelColor(midStrip + a+b , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=midStrip; k < 39; k+=3) {
        strip.setPixelColor(k+b, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=midStrip; k < 39; k+=5) {
        strip.setPixelColor(k+b, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

 /*The splash, to the left of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip -a-2, 160,160,160);
      strip.setPixelColor(midStrip -a-1, 190,190,190);
      strip.setPixelColor(midStrip -a, 255,255,255);
      strip.setPixelColor(midStrip-a+1, 190,190,190);
      strip.setPixelColor(midStrip-a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a >= 7)
      {
        delay(100);
        leftSplash = false;
      }

}

/*right splash*/
void splashRight(int a, int b)
{
  /*Where the splash should start*/
  int midStrip = 29;

  strip.setBrightness(30);

  /*Background colour on right side of middle, underneath splash*/
  for(int i = 0; i < 10;i++)
  {
    strip.setPixelColor(midStrip + i, 30,144,255); 
  }
      /*The left side, no splash*/
         
      for (uint16_t k=midStrip; k > 19; k--) {
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=midStrip; k > 19; k-=2) {
        strip.setPixelColor(midStrip - a-b , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=midStrip; k > 19; k-=3) {
        strip.setPixelColor(k-b, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=midStrip; k > 19; k-=5) {
        strip.setPixelColor(k-b, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

    

 /*The splash, to the right of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better and can be seen in waterstreamcode.ino
 , but it simply did not work.)*/
      strip.setPixelColor(midStrip +a-2, 160,160,160);
      strip.setPixelColor(midStrip +a-1, 190,190,190);
      strip.setPixelColor(midStrip +a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 190,190,190);
      strip.setPixelColor(midStrip+a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a >= 7)
     {
        delay(100);
        rightSplash = false;
      }
        /* 
       */

}

void splashUpDown(int a, int b)
{
    /*Where the splash should start*/
    int midStrip = 29;

    strip.setBrightness(30);
    
    /*Background colour on right side of middle, underneath splash*/
    for(int i = 0; i < 20;i++)
    {
      strip.setPixelColor(19 + i, 30,144,255); 
    }

    /*Splashes, to left and right*/
     /*The splash, to the right of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip +a-2, 160,160,160);
      strip.setPixelColor(midStrip +a-1, 190,190,190);
      strip.setPixelColor(midStrip +a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 190,190,190);
      strip.setPixelColor(midStrip+a+2, 160,160,160);

       /*The splash, to the left of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip -a-2, 160,160,160);
      strip.setPixelColor(midStrip -a-1, 190,190,190);
      strip.setPixelColor(midStrip -a, 255,255,255);
      strip.setPixelColor(midStrip-a+1, 190,190,190);
      strip.setPixelColor(midStrip-a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a >= 7)
      {
        delay(100);
        upOrDownSplash = false;
      }
      
}
