#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0,0,0);
    ofDisableAlphaBlending();
    ofEnableDepthTest();
    cam.setPosition(0,0,40);
    
    campos = 1;
    
     /*Image by <a href="https://pixabay.com/users/alkemade-804941/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">alkemade</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4465800">Pixabay</a>*/
    ofDisableArbTex();
    ofLoadImage(tex,"sea.jpeg");
    
    int meshX = 50;
    int meshY = 50;
    int spacing = 1;
    
    //for loop to add vertices to the mesh -- how big the size of one square should be
    for(int i = -meshX/2; i < meshX/2; i++)
    {
        for(int j = -meshY/2; j <meshY/2; j++)
        {
            mesh.addVertex(ofPoint(i*spacing, j*spacing, 0));
            mesh.addTexCoord(ofVec2f(i*spacing, j*spacing));
        }
    }
    
    // Set up triangles' indices
    // only loop to -1 so they don't connect back to the beginning of the row
    for(int x = 0; x < meshX - 1; x++) {
        for(int y = 0; y < meshY - 1; y++) {
            int topLeft     = x   + meshX * y;
            int bottomLeft  = x+1 + meshX * y;
            int topRight    = x   + meshX * (y+1);
            int bottomRight = x+1 + meshX * (y+1);

            mesh.addTriangle( topLeft, bottomLeft, topRight );
            mesh.addTriangle( bottomLeft, topRight, bottomRight );

        }
    }
    
}

//--------------------------------------------------------------
void ofApp::update(){
    for(int i = 0;i < mesh.getVertices().size(); i++)
    {
        float x = mesh.getVertex(i).x;
        float y = mesh.getVertex(i).y;
        mesh.setVertex(i, ofVec3f(x,y,getZNoiseValue(x, y)));
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    cam.begin();
    ofEnableDepthTest();
    tex.bind();
        mesh.draw();
    tex.unbind();
    cam.end();
}

//--------------------------------------------------------------
//adding a noise function to return a float value and make a more flowy mesh
float ofApp::getZNoiseValue(float x, float y) {
    float time = ofGetFrameNum() * 0.005;
    float n1 = ofNoise(x * 0.06, y * 0.005, time) * 5;
    float n2 = ofNoise(x * 0.005, y * 0.008, time) * 7;
    return n1 + n2;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    //depening on user input the camera would move -- w and s for zoom in and zoom out
    //a and d for left and right
    if(key == 119)//'w'
    {
        cam.dolly(-campos);
    }
    if(key == 115)//s
    {
        cam.dolly(campos);
    }
    if(key == 97)//a
    {
        cam.panDeg(-campos);
    }
    if(key == 100)//d
    {
        cam.panDeg(campos);
    }
    if(key == 'z')
    {
        cam.tiltDeg(campos*2);
    }
    if(key == 'x')
    {
        cam.tiltDeg(-campos*2);
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
