#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

//#define PIN 6
int ledPin = 5;
bool state = false;

//Animations, reactions to splashes
//If gesture sensor captures certain movements
bool rightSplash = false;
bool leftSplash = false;
bool upOrDownSplash = true;


Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, ledPin, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
}

void loop() {
  
        /*Run the water stream*/
        waterStream(100);

}

void waterStream(uint8_t wait) {



  strip.setBrightness(30);
  
/*Splitting strip into sections, 
 * the two side sections can continue the same way throughtout but 
 * the middle one should be manipulated when a user moves their hand over the gesture sensor.
  */
  int leftSection = 19;
  int middleSection = 39;
  int rightSection = 59;
  
  /*Stream loops - based on iceflakes example, candyChase*/
  for (int j=0; j<10; j++) {  
    //q is lower no than amount of colour varitations to give a more random/natural appearacnce*/
    for (int q=0; q < 3; q++) {
      
      /*Left section*/
      for (uint16_t i=0; i < strip.numPixels()/3; i++) {
        strip.setPixelColor(i, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=2) {
        strip.setPixelColor(j+q , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=3) {
        strip.setPixelColor(i+q, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t i=0; i < strip.numPixels()/3; i+=5) {
        strip.setPixelColor(i+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

    /*right section*/
      for (uint16_t k=39; k < strip.numPixels(); k++) {
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=2) {
        strip.setPixelColor(40 + j+q , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=3) {
        strip.setPixelColor(k+q, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=39; k < strip.numPixels(); k+=5) {
        strip.setPixelColor(k+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }


  /*middle section*/
  if(leftSplash == false && rightSplash == false && upOrDownSplash == false)
  {
       strip.setBrightness(30);
      for (uint16_t k=19; k < 39; k++) {
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=19; k < 39; k+=2) {
        strip.setPixelColor(19 + j+q , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=19; k < 39; k+=3) {
        strip.setPixelColor(k+q, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=19; k < 39; k+=5) {
        strip.setPixelColor(k+q, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }
    }
    else if(leftSplash == true)
    {  
      splashLeft(j, q);
    }
    else if(rightSplash == true)
    {
      splashRight(j, q);
    }
    else if(upOrDownSplash == true)
    {
      splashUpDown(j, q);
    }
  
         strip.show();

      delay(wait);
     }
  }
}


/*Splash animation functions*/
/*left splash*/
void splashLeft(int a, int b)
{
  /*Where the splash should start*/
  int midStrip = 29;

  /*Background colour on left side of middle, underneath splash*/
  for(int i = 0; i < 10;i++)
  {
    strip.setPixelColor(midStrip - i, 30,144,255); 
  }
      /*The right side, no splash*/
         strip.setBrightness(30);
      for (uint16_t k=midStrip; k < 39; k++) {
        //strip.setBrightness(30);
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=midStrip; k < 39; k+=2) {
        //strip.setBrightness(30);
        strip.setPixelColor(midStrip + a+b , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=midStrip; k < 39; k+=3) {
        //strip.setBrightness(30);
        strip.setPixelColor(k+b, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=midStrip; k < 39; k+=5) {
       // strip.setBrightness(30);
        strip.setPixelColor(k+b, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

 /*The splash, to the left of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip -a-2, 160,160,160);
      strip.setPixelColor(midStrip -a-1, 190,190,190);
      strip.setPixelColor(midStrip -a, 255,255,255);
      strip.setPixelColor(midStrip-a+1, 190,190,190);
      strip.setPixelColor(midStrip-a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a == 9)
      {
        delay(100);
        leftSplash = false;
      }


}

/*right splash*/
void splashRight(int a, int b)
{
  /*Where the splash should start*/
  int midStrip = 29;

  /*Background colour on right side of middle, underneath splash*/
  for(int i = 0; i < 10;i++)
  {
    strip.setPixelColor(midStrip + i, 30,144,255); 
  }
      /*The left side, no splash*/
         strip.setBrightness(30);
      for (uint16_t k=midStrip; k > 19; k--) {
        //strip.setBrightness(30);
        strip.setPixelColor(k, 30,144,255);    //turn every pixel blue
      }
      for (uint16_t k=midStrip; k > 19; k-=2) {
        //strip.setBrightness(30);
        strip.setPixelColor(midStrip - a-b , 0,0,205);    //turn every third pixel medium blue
      }
      for (uint16_t k=midStrip; k > 19; k-=3) {
        //strip.setBrightness(30);
        strip.setPixelColor(k-b, 0,0,205 );    //turn every fifth pixel medium blue
      }
      for (uint16_t k=midStrip; k > 19; k-=5) {
       // strip.setBrightness(30);
        strip.setPixelColor(k-b, 173,216,230);    //turn every sixth pixel aliceblue, almost white
      }

    

 /*The splash, to the right of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip +a-2, 160,160,160);
      strip.setPixelColor(midStrip +a-1, 190,190,190);
      strip.setPixelColor(midStrip +a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 190,190,190);
      strip.setPixelColor(midStrip+a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a == 9)
      {
        rightSplash = false;
      }

      /*Hardcoded version of splash that looked better, but was too much for the LED strip/Arduino*/
  /*if(a == 0)
  {
      strip.setPixelColor(midStrip + a, 255,255,255);
      strip.setPixelColor(midStrip +a +1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
      strip.setPixelColor(midStrip+a+3, 160,160,160);
  }
  else if(a == 1)
  {
      strip.setPixelColor(midStrip +a -2, 190,190,190);
      strip.setPixelColor(midStrip +a -1, 220,220,220);
      strip.setPixelColor(midStrip + a, 255,255,255);
      strip.setPixelColor(midStrip +a +1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
      strip.setPixelColor(midStrip+a+3, 160,160,160);
      strip.setPixelColor(midStrip+a+4, 130,130,130);
  }
  else if(a == 2)
  {
      strip.setPixelColor(midStrip+a-3, 160,160,160);
      strip.setPixelColor(midStrip+a-2, 190,190,190);
      strip.setPixelColor(midStrip+a -1, 220,220,220);
      strip.setPixelColor(midStrip+a, 255,255,255);
      strip.setPixelColor(midStrip+a +1, 255,255,255);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
      strip.setPixelColor(midStrip+a+3, 160,160,160);
      strip.setPixelColor(midStrip+a+4, 130,130,130);
  }
  else if(a == 3)
  {
      strip.setPixelColor(midStrip+a-3, 130,130,130);
      strip.setPixelColor(midStrip+a-2, 190,190,190);
      strip.setPixelColor(midStrip+a -1, 220,220,220);
      strip.setPixelColor(midStrip+a, 255,255,255);
      strip.setPixelColor(midStrip+a +1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
      strip.setPixelColor(midStrip+a+3, 160,160,160);
      strip.setPixelColor(midStrip+a+4, 160,160,160);
      strip.setPixelColor(midStrip+a+5, 130,130,130);
  }
  else if(a == 4)
  {
      strip.setPixelColor(midStrip+a-4, 130,130,130);
      strip.setPixelColor(midStrip+a-3, 130,130,130);
      strip.setPixelColor(midStrip+a-2, 190,190,190);
      strip.setPixelColor(midStrip+a-1, 220,220,220);
      strip.setPixelColor(midStrip+a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
      strip.setPixelColor(midStrip+a+3, 160,160,160);
      strip.setPixelColor(midStrip+a+4, 160,160,160);
  }
  else if(a == 5)
  {
      strip.setPixelColor(midStrip+a-5, 70,70,70);
      strip.setPixelColor(midStrip+a-4, 100,100,100);
      strip.setPixelColor(midStrip+a-3, 130,130,130);
      strip.setPixelColor(midStrip+a-2, 190,190,190);
      strip.setPixelColor(midStrip+a-1, 220,220,220);
      strip.setPixelColor(midStrip+a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 190,190,190);
  }
  else if(a == 6)
  {
      strip.setPixelColor(midStrip+a-5, 40,40,40);
      strip.setPixelColor(midStrip+a-4, 70,70,70);
      strip.setPixelColor(midStrip+a-3, 100,100,100);
      strip.setPixelColor(midStrip+a-2, 160,160,160);
      strip.setPixelColor(midStrip+-a-1, 190,190,190);
      strip.setPixelColor(midStrip+a, 255,255,255);
      strip.setPixelColor(midStrip+a +1, 220,220,220);
      strip.setPixelColor(midStrip+a+2, 220,220,220);
  }
  else if(a == 7)
  {
      strip.setPixelColor(midStrip+a-4, 70,70,70);
      strip.setPixelColor(midStrip+a-3, 130,130,130);
      strip.setPixelColor(midStrip+a-2, 160,160,160);
      strip.setPixelColor(midStrip+a-1, 190,190,190);
      strip.setPixelColor(midStrip+a, 220,220,220);
      strip.setPixelColor(midStrip+a +1, 220,220,220);
  }
  else if(a == 8)
  {
      strip.setPixelColor(midStrip+a -1, 70,70,70);
      strip.setPixelColor(midStrip+a, 130,130,130);
      strip.setPixelColor(midStrip+a +1, 190,190,190);
      strip.setPixelColor(midStrip+a+2, 110,110,110);
  }
  else if(a == 9)
  {
      strip.setPixelColor(midStrip+a-1, 40,40,40);
      strip.setPixelColor(midStrip+a, 70,70,70);
      strip.setPixelColor(midStrip+a+1, 100,100,100);
      strip.setPixelColor(midStrip+a+2, 70,70,70);
      leftSplash = false;
  }*/

}

void splashUpDown(int a, int b)
{
    /*Where the splash should start*/
    int midStrip = 29;
    
    /*Background colour on right side of middle, underneath splash*/
    for(int i = 0; i < 20;i++)
    {
      strip.setPixelColor(19 + i, 30,144,255); 
    }

    /*Splashes, to left and right*/
     /*The splash, to the right of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip +a-2, 160,160,160);
      strip.setPixelColor(midStrip +a-1, 190,190,190);
      strip.setPixelColor(midStrip +a, 255,255,255);
      strip.setPixelColor(midStrip+a+1, 190,190,190);
      strip.setPixelColor(midStrip+a+2, 160,160,160);

       /*The splash, to the left of the middle (Simplified code to avoid spike in power, 
 hardcoded version that looks a bit better below, but it simply did not work.)*/
      strip.setPixelColor(midStrip -a-2, 160,160,160);
      strip.setPixelColor(midStrip -a-1, 190,190,190);
      strip.setPixelColor(midStrip -a, 255,255,255);
      strip.setPixelColor(midStrip-a+1, 190,190,190);
      strip.setPixelColor(midStrip-a+2, 160,160,160);

      /*Switch the splash off when the full splash has been made*/
      if(a == 9)
      {
        delay(100);
        upOrDownSplash = false;
      }
      
}
