// Code to send and receive data from openFrameworks
bool ledState = false; // Variable for storing the led state
int ledPin = 13;

void setup() {
  // Start the serial communication

  pinMode(ledPin, OUTPUT);  // Declaring the LED pin as output
   Serial.begin(9600);
}

void loop() {
  if (Serial.available()) { // If there is any data available
    char inByte = Serial.read(); // store the incoming data
    if (inByte == 1) {    // Whether the received data is '1'
      ledState = !ledState; // Change the LED state
      digitalWrite(ledPin, ledState); // Turn the LED
    }

    if (ledState == true) {
      char mystr[10] = "1";
      Serial.write(mystr, 10); // send the response in return
    }
    else if (ledState == false) {
      char mystr[10] = "0";      
      Serial.write(mystr, 10); // send the response in return
    }
  }
}
