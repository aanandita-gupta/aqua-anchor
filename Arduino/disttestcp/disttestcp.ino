/*
 * Posted on https://randomnerdtutorials.com
 * created by http://playground.arduino.cc/Code/NewPing
*/

#include <NewPing.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_APDS9960.h>
 
#define TRIGGER_PIN 11
#define ECHO_PIN 12
#define MAX_DISTANCE 200

// NewPing setup of pins and maximum distance
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

 /*GESTURE SENSOR*/
Adafruit_APDS9960 apds;

/*Set up led to be triggered by dist sensor*/
int led = 13;
int blue_led = 10;

/*Set up led strip*/
#define LED_PIN            6

/*How many  led pixels*/
 #define NUMPIXELS      10

 // When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, LED_PIN, NEO_RGB + NEO_KHZ800);

int delayval = 200; // delay for half a second

/*Implement timestamp so that gesture sensor is also checked*/
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change:
const long interval = 2000;           // interval at which to check distance sensor

void setup() {
   Serial.begin(9600);

   //pinMode(led, OUTPUT); // Declare the LED as an output

   //pixels.begin(); // This initializes the NeoPixel library.

   /*GESTURE SENSOR*/
 if(!apds.begin()){
    Serial.println("failed to initialize device! Please check your wiring.");
  }
  else Serial.println("Device initialized!");

    //gesture mode will be entered once proximity mode senses something close
  apds.enableProximity(true);
  apds.enableGesture(true);
}
 
void loop() {
   //delay(50);


   // check to see if it's time to check distance; that is, if the difference
  // between the current time and last time you checked distanceD is bigger than
  // the interval at which you want to check distance.
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    
   previousMillis = currentMillis;

   unsigned int distance = sonar.ping_cm();

   
     /*Trigger led strip if distance is less than 10 cm*/
   if(distance < 10)
   {

      Serial.println("lights on");
      digitalWrite(blue_led, HIGH);
      

      //for(int i=0;i<NUMPIXELS;i++){

      // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      //pixels.setPixelColor(i, pixels.Color(0,0,200)); // Moderately bright green color.

      //pixels.show(); // This sends the updated pixel color to the hardware.

      //delay(delayval); // Delay for a period of time (in milliseconds).

     //}
   }
   else
   {
      Serial.println("lights off");
      digitalWrite(blue_led, LOW); // Turn the LED on
    
    //  Serial.println("sound off");
      //pixels.clear(); // Set all pixel colors to 'off'
   }
  }
  
 //  Serial.print(distance);
 //  Serial.println("cm");
    //pixels.clear(); // Set all pixel colors to 'off'
  
      //digitalWrite(led, HIGH); // Turn the LED on
      /*  for(int i=0;i<10;i++){

      // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      pixels.setPixelColor(i, pixels.Color(0,0,200)); // Moderately bright green color.

      pixels.show(); // This sends the updated pixel color to the hardware.

      delay(delayval); // Delay for a period of time (in milliseconds).

     }*/
  

       //read a gesture from the device
    uint8_t gesture = apds.readGesture();
    if(gesture == APDS9960_UP || gesture == APDS9960_DOWN || gesture == APDS9960_LEFT || gesture == APDS9960_RIGHT)
    {
      Serial.println("water splash");
      digitalWrite(led, HIGH);

      /*for(int i=0;i<NUMPIXELS;i++){

      // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      if(i%3 > 1)
      {
        
        pixels.setPixelColor(i, pixels.Color(0,0,150)); // Moderately bright green color.

      }
      else
      {
        pixels.setPixelColor(i, pixels.Color(150,150,150)); // Moderately bright green color.
 
      }
      pixels.show(); // This sends the updated pixel color to the hardware.

      delay(delayval); // Delay for a period of time (in milliseconds).

     }*/
      
    }
    else
    {
      digitalWrite(led, LOW);
    }
     /*   if(gesture == APDS9960_DOWN)
    {
      Serial.println("water splash");
    }
    if(gesture == APDS9960_LEFT) 
    {
      Serial.println("water splash");
    }
    if(gesture == APDS9960_RIGHT)
    {
      Serial.println("water splash");
    }*/




   
}
